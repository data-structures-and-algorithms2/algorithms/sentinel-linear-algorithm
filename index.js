const a = ['F', 'C', 'K', 'A', 'Z']; // haystack
const x = 'Z'; // needle
const n = a.length - 1; // last index of array
const t = a[n]; // Keep last element as temp
a[n] = x; // Replace last element with needle
let i = 0; // First index
do {
  if (a[i] === x) {
    if (i === n) {
      // if found index is last one, result maybe false positive 
      if (t !== x) {
        console.log(`Axtardığımız hərf ( ${x} ) verilən massivdə tapılmadı.`);
        break;
      }
    }
    console.log(`Axtardığımız hərfin ( ${x} ) indeksi ${i}`);
    break;
  }
} while (a[i++] !== x);
